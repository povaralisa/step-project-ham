"use strict"
document.getElementById("section-list-ul").addEventListener("click", function(event) {
    if (event.target.tagName === "LI") {
        document.getElementsByClassName("list-active")[0].classList.toggle("list-active");
        event.target.classList.toggle("list-active");

        document.getElementsByClassName("section-three")[0].classList.toggle("section-three");
        document.querySelector(`.switch-tab-shown[data-tab=${event.target.dataset.tab}]`).classList.toggle("section-three");
    }
});


const filterByCollection = function(collection) {
    const allCards = Array.from(document.getElementsByClassName("filter-card"));
    allCards.forEach(item => item.classList.remove("filter-card-shown"));

    Array.from(collection).forEach(item => item.classList.add("filter-card-shown"));
}

const getCollectionByFilterClass = function(filter) {
    const allCards = Array.from(document.getElementsByClassName("filter-card"));

    return allCards.filter(item => item.dataset.filter.includes(filter));
}